#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/usb/input.h>
#include <linux/hid.h>
#include <linux/jiffies.h>
#include <linux/time.h>

MODULE_LICENSE("Dual BSD/GPL");

struct mouse {
	char name[128];
	char phys[64];
	struct usb_device *usbdev;
	struct input_dev *dev;
	struct urb *irq;
	signed char *data;
	dma_addr_t data_dma;

	spinlock_t lock;
	unsigned long prev_jiffies;
	struct timespec64 *ts;
	unsigned int ts_size;

	unsigned int start_logging;
	unsigned int log_enabled;
};

static const unsigned int limit = PAGE_SIZE / sizeof(struct timespec64);

static ssize_t log_enabled_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	struct mouse *mouse = dev_get_drvdata(dev);

	return snprintf(buf, PAGE_SIZE, "%u\n", mouse->log_enabled);
}

static ssize_t log_enabled_store(struct device *dev,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	struct mouse *mouse = dev_get_drvdata(dev);
	unsigned int value;
	unsigned long flags;

	if (kstrtouint(buf, 0 /* base */, &value))
		return -EINVAL;

	spin_lock_irqsave(&mouse->lock, flags);
	mouse->log_enabled = value;
	if (mouse->log_enabled) {
		mouse->ts_size = 0;
		mouse->start_logging = 1;
	}
	spin_unlock_irqrestore(&mouse->lock, flags);

	dev_info(&mouse->usbdev->dev, "%s click event diffs logging\n",
		mouse->log_enabled ? "enabling" : "disabling");

	return count;
}

static ssize_t click_event_diffs_show(struct device *dev,
				      struct device_attribute *attr, char *buf)
{
	struct mouse *mouse = dev_get_drvdata(dev);
	const unsigned int sysfs_limit = min(mouse->ts_size,
		(unsigned int)PAGE_SIZE / 21 /* timespec format size */);
	char *str = buf;
	int i;

	for (i = 0; i < sysfs_limit; i++)
		str += sprintf(str, "%10i.%09i\n", (int)mouse->ts[i].tv_sec,
			       (int)mouse->ts[i].tv_nsec);

	return str - buf;
}

static DEVICE_ATTR_RW(log_enabled);
static DEVICE_ATTR_RO(click_event_diffs);

static struct attribute *mouse_device_attrs[] = {
	&dev_attr_log_enabled.attr,
	&dev_attr_click_event_diffs.attr,
	NULL
};

ATTRIBUTE_GROUPS(mouse_device);

static void store_timespec(struct mouse *mouse)
{
	unsigned long flags, j = jiffies;

	spin_lock_irqsave(&mouse->lock, flags);
	if (mouse->start_logging) {
		mouse->start_logging = 0;
	} else {
		/* XXX: is it safe to call this under a spinlock? */
		jiffies_to_timespec64((long)j - (long)mouse->prev_jiffies,
				      mouse->ts + mouse->ts_size++);
		if (mouse->ts_size >= limit) /* log overflow */
			mouse->ts_size = 0;
	}
	spin_unlock_irqrestore(&mouse->lock, flags);

	mouse->prev_jiffies = j;
}

static void mouse_irq(struct urb *urb)
{
	struct mouse *mouse = urb->context;
	signed char *data = mouse->data;
	struct input_dev *dev = mouse->dev;
	int status;

	switch (urb->status) {
	case 0: /* success */
		break;
	case -ECONNRESET: /* unlink */
	case -ENOENT:
	case -ESHUTDOWN:
		return;
	/* -EPIPE:  should clear the halt */
	default: /* error */
		goto resubmit;
	}

	if (mouse->log_enabled && data[0] & 0x01)
		store_timespec(mouse);

	input_report_key(dev, BTN_LEFT,   data[0] & 0x01);
	input_report_key(dev, BTN_RIGHT,  data[0] & 0x02);
	input_sync(dev);

resubmit:
	status = usb_submit_urb(urb, GFP_ATOMIC);
	if (status)
		dev_err(&mouse->usbdev->dev,
			"can't resubmit intr, %s-%s/input0, status %d\n",
			mouse->usbdev->bus->bus_name,
			mouse->usbdev->devpath, status);
}

static int mouse_open(struct input_dev *dev)
{
	struct mouse *mouse = input_get_drvdata(dev);

	mouse->irq->dev = mouse->usbdev;
	if (usb_submit_urb(mouse->irq, GFP_KERNEL))
		return -EIO;

	return 0;
}

static void mouse_close(struct input_dev *dev)
{
	struct mouse *mouse = input_get_drvdata(dev);

	usb_kill_urb(mouse->irq);
}

static int mouse_probe(struct usb_interface *intf, const struct usb_device_id *id)
{
	struct usb_device *dev = interface_to_usbdev(intf);
	struct usb_host_interface *interface;
	struct usb_endpoint_descriptor *endpoint;
	struct mouse *mouse;
	struct input_dev *input_dev;
	int pipe, maxp;
	int retval = -ENOMEM;

	interface = intf->cur_altsetting;
	if (interface->desc.bNumEndpoints != 1)
		return -ENODEV;

	endpoint = &interface->endpoint[0].desc;
	if (!usb_endpoint_is_int_in(endpoint))
		return -ENODEV;

	/* specifies an interrupt IN endpoint */
	pipe = usb_rcvintpipe(dev, endpoint->bEndpointAddress);
	maxp = usb_maxpacket(dev, pipe, usb_pipeout(pipe));

	mouse = kzalloc(sizeof(struct mouse), GFP_KERNEL);
	if (!mouse)
		return retval;
	mouse->ts = (void *)get_zeroed_page(GFP_KERNEL);
	if (!mouse->ts)
		goto free_mouse;

	input_dev = input_allocate_device();
	if (!input_dev)
		goto free_page;

	/* allocate dma-consistent buffer for URB_NO_xxx_DMA_MAP */
	mouse->data = usb_alloc_coherent(dev, 8 /* buffer_size */, GFP_KERNEL,
					&mouse->data_dma);
	if (!mouse->data)
		goto free_input_dev;
	mouse->irq = usb_alloc_urb(0 /* iso_packets */, GFP_KERNEL);
	if (!mouse->irq)
		goto free_usb_coherent;

	mouse->usbdev = dev;
	mouse->dev = input_dev;

	spin_lock_init(&mouse->lock);

	if (dev->manufacturer)
		strscpy(mouse->name, dev->manufacturer, sizeof(mouse->name));

	if (dev->product) {
		if (dev->manufacturer)
			strlcat(mouse->name, " ", sizeof(mouse->name));
		strlcat(mouse->name, dev->product, sizeof(mouse->name));
	}

	if (!strlen(mouse->name))
		snprintf(mouse->name, sizeof(mouse->name),
			"USB HIDBP Mouse %04x:%04x",
			le16_to_cpu(dev->descriptor.idVendor),
			le16_to_cpu(dev->descriptor.idProduct));

	/* returns stable device path in the usb tree */
	usb_make_path(dev, mouse->phys, sizeof(mouse->phys));
	strlcat(mouse->phys, "/input0", sizeof(mouse->phys));

	input_dev->name = mouse->name;
	input_dev->phys = mouse->phys;
	usb_to_input_id(dev, &input_dev->id);
	input_dev->dev.parent = &intf->dev;

	input_dev->evbit[0] = BIT_MASK(EV_KEY);
	input_dev->keybit[BIT_WORD(BTN_MOUSE)] = BIT_MASK(BTN_LEFT) |
		BIT_MASK(BTN_RIGHT);

	input_set_drvdata(input_dev, mouse);

	input_dev->open = mouse_open;
	input_dev->close = mouse_close;
	input_dev->dev.groups = mouse_device_groups;

	usb_fill_int_urb(mouse->irq, dev, pipe, mouse->data,
			(maxp > 8 ? 8 : maxp) /*buffer length*/,
			mouse_irq, mouse, endpoint->bInterval);
	mouse->irq->transfer_dma = mouse->data_dma;
	mouse->irq->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;

	retval = input_register_device(mouse->dev);
	if (retval)
		goto free_usb_urb;

	usb_set_intfdata(intf, mouse);
	return 0;

free_usb_urb:
	usb_free_urb(mouse->irq);
free_usb_coherent:
	usb_free_coherent(dev, 8, mouse->data, mouse->data_dma);
free_input_dev:
	input_free_device(input_dev);
free_page:
	free_page((unsigned long)mouse->ts);
free_mouse:
	kfree(mouse);
	return retval;
}

static void mouse_disconnect(struct usb_interface *intf)
{
	struct mouse *mouse = usb_get_intfdata(intf);

	usb_set_intfdata(intf, NULL);
	if (mouse) {
		usb_kill_urb(mouse->irq);
		input_unregister_device(mouse->dev);
		usb_free_urb(mouse->irq);
		usb_free_coherent(interface_to_usbdev(intf), 8, mouse->data, mouse->data_dma);
		free_page((unsigned long)mouse->ts);
		kfree(mouse);
	}
}

static const struct usb_device_id mouse_id_table[] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID, USB_INTERFACE_SUBCLASS_BOOT,
		USB_INTERFACE_PROTOCOL_MOUSE) },
	{ } /* Terminating entry */
};

MODULE_DEVICE_TABLE(usb, mouse_id_table);

static struct usb_driver mouse_driver = {
	.name		= "usb_mouse",
	.probe		= mouse_probe,
	.disconnect	= mouse_disconnect,
	.id_table	= mouse_id_table,
};

module_usb_driver(mouse_driver);
